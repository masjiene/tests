FROM httpd:2.4

MAINTAINER Francois van der Merwe (swyser.co.za)

COPY /dist/ /usr/local/apache2/htdocs/

# COPY /config/server/apache/certs/server.crt /usr/local/apache2/conf/
# COPY /config/server/apache/certs/server.key /usr/local/apache2/conf/
# COPY /config/server/apache/certs/server.ca-bundle.crt /usr/local/apache2/conf/
# COPY /config/server/apache/certs/httpd.conf /usr/local/apache2/conf/
# COPY /config/server/apache/certs/httpd-ssl.conf /usr/local/apache2/conf/extra/

# Exposed ports
EXPOSE 80
EXPOSE 443

# docker run -p 443:443/tcp -p 80:80/tcp -dit swyserapp
